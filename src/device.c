/*
 * /net/reactivated/Fprint/Device/foo object implementation
 * Copyright (C) 2008 Daniel Drake <dsd@gentoo.org>
 * Copyright (C) 2020 Marco Trevisan <marco.trevisan@canonical.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#include <glib/gi18n.h>
#include <gio/gio.h>
#include <polkit/polkit.h>
#include <fprint.h>

#include <sys/types.h>
#include <pwd.h>
#include <errno.h>

#include "fprintd.h"
#include "storage.h"

static const char *FINGERS_NAMES[] = {
	[FP_FINGER_UNKNOWN] = "unknown",
	"left-thumb",
	"left-index-finger",
	"left-middle-finger",
	"left-ring-finger",
	"left-little-finger",
	"right-thumb",
	"right-index-finger",
	"right-middle-finger",
	"right-ring-finger",
	"right-little-finger"
};

static void fprint_device_dbus_skeleton_iface_init (FprintDBusDeviceIface *);
static gboolean action_authorization_handler (GDBusInterfaceSkeleton *,
					      GDBusMethodInvocation *,
					      gpointer user_data);

static GQuark quark_auth_user = 0;

typedef enum {
	ACTION_NONE = 0,
	ACTION_IDENTIFY,
	ACTION_VERIFY,
	ACTION_ENROLL,
	ACTION_OPEN,
	ACTION_CLOSE,
} FprintDeviceAction;

typedef enum {
	STATE_CLAIMED,
	STATE_UNCLAIMED,
	STATE_IGNORED,
} FprintDeviceClaimState;

typedef struct {
	volatile gint _refcount;

	/* current method invocation */
	GDBusMethodInvocation *invocation;

	/* The current user of the device, if claimed */
	const char * const sender;

	/* The current user of the device, or if allowed,
	 * what was passed as a username argument */
	const char * const username;

	gboolean verify_status_reported;
} SessionData;

typedef struct {
	guint32 id;
	FpDevice *dev;
	SessionData *_session;

	PolkitAuthority *auth;

	/* Hashtable of connected clients */
	GHashTable *clients;

	/* Required to restart the operation on a retry failure. */
	FpPrint   *verify_data;
	GPtrArray *identify_data;
	int enroll_data;

	/* whether we're running an identify, or a verify */
	FprintDeviceAction current_action;
	GCancellable *current_cancellable;
	GDBusMethodInvocation *current_cancel_invocation;
} FprintDevicePrivate;

G_DEFINE_TYPE_WITH_CODE (FprintDevice, fprint_device,
			 FPRINT_DBUS_TYPE_DEVICE_SKELETON,
			 G_ADD_PRIVATE (FprintDevice)
			 G_IMPLEMENT_INTERFACE (FPRINT_DBUS_TYPE_DEVICE,
			 			fprint_device_dbus_skeleton_iface_init));

enum fprint_device_properties {
	FPRINT_DEVICE_CONSTRUCT_DEV = 1,
	FPRINT_DEVICE_IN_USE,
	FPRINT_DEVICE_NAME,
	FPRINT_DEVICE_NUM_ENROLL,
	FPRINT_DEVICE_SCAN_TYPE
};

enum fprint_device_signals {
	SIGNAL_VERIFY_STATUS,
	SIGNAL_VERIFY_FINGER_SELECTED,
	SIGNAL_ENROLL_STATUS,
	NUM_SIGNALS,
};

static guint32 last_id = ~0;
static guint signals[NUM_SIGNALS] = { 0, };

static void
session_data_unref(SessionData *session)
{
	if (g_atomic_int_dec_and_test (&session->_refcount)) {
		g_clear_pointer((char**) &session->sender, g_free);
		g_clear_pointer((char**) &session->username, g_free);
		g_clear_object (&session->invocation);
		g_free(session);
	}
}
G_DEFINE_AUTOPTR_CLEANUP_FUNC(SessionData, session_data_unref);

static SessionData*
session_data_get (FprintDevicePrivate *priv)
{
	SessionData *invalid = (SessionData*) &priv->_session;
	SessionData *cur;

	/* Get the current pointer and mark the pointer as "busy". */
	do {
		cur = priv->_session;
		/* Swap if cur is valid, otherwise busy loop. */
	} while (cur == invalid || !g_atomic_pointer_compare_and_exchange (&priv->_session, cur, invalid));

	/* We can safely increase the reference count now. */
	if (cur)
		g_atomic_int_inc (&cur->_refcount);

	/* Swap back, this must succeed. */
	if (!g_atomic_pointer_compare_and_exchange (&priv->_session, invalid, cur))
		g_assert_not_reached ();

	return cur;
}

/* Pass NULL sender and username to unset session data. */
static SessionData*
session_data_set_new (FprintDevicePrivate *priv, gchar *sender, gchar *username)
{
	SessionData *invalid = (SessionData*) &priv->_session;
	SessionData *new = NULL;
	SessionData *old;

	g_assert ((!sender && !username) || (sender && username));
	if (sender) {
		new = g_new0 (SessionData, 1);
		/* Internal reference of the pointer and returned reference. */
		new->_refcount = 2;
		*(char**) &new->sender = sender;
		*(char**) &new->username = username;
	}

	/* Get the current (but not if it is busy) and put the new one in place. */
	do {
		old = priv->_session;
		/* Swap if old is valid, otherwise busy loop as someone is ref'ing it currently. */
	} while (old == invalid || !g_atomic_pointer_compare_and_exchange (&priv->_session, old, new));

	/* We can safely drop the our internal reference now. */
	if (old)
		session_data_unref (old);

	return new;
}

static void fprint_device_dispose(GObject *object)
{
	FprintDevice *self = (FprintDevice *) object;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(self);

	g_hash_table_remove_all (priv->clients);

	G_OBJECT_CLASS(fprint_device_parent_class)->dispose(object);
}

static void fprint_device_finalize(GObject *object)
{
	FprintDevice *self = (FprintDevice *) object;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(self);

	g_hash_table_destroy (priv->clients);
	session_data_set_new(priv, NULL, NULL);
	g_clear_object (&priv->auth);
	g_clear_object (&priv->dev);

	if (priv->current_action != ACTION_NONE ||
	    priv->_session ||
	    priv->verify_data ||
	    priv->identify_data ||
	    priv->current_cancellable ||
	    priv->current_cancel_invocation)
		g_critical("Device was not cleaned up properly before being finalized.");

	G_OBJECT_CLASS(fprint_device_parent_class)->finalize(object);
}

static void fprint_device_set_property(GObject *object, guint property_id,
	const GValue *value, GParamSpec *pspec)
{
	FprintDevice *self = (FprintDevice *) object;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(self);

	switch (property_id) {
	case FPRINT_DEVICE_CONSTRUCT_DEV:
		priv->dev = g_value_dup_object(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
		break;
	}
}

static void fprint_device_get_property(GObject *object, guint property_id,
				       GValue *value, GParamSpec *pspec)
{
	FprintDevice *self = (FprintDevice *) object;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(self);

	switch (property_id) {
	case FPRINT_DEVICE_CONSTRUCT_DEV:
		g_value_set_object(value, priv->dev);
		break;
	case FPRINT_DEVICE_IN_USE:
		g_value_set_boolean(value, g_hash_table_size (priv->clients) != 0);
		break;
	case FPRINT_DEVICE_NAME:
		g_value_set_static_string (value, fp_device_get_name (priv->dev));
		break;
	case FPRINT_DEVICE_NUM_ENROLL:
		if (priv->dev)
			g_value_set_int (value, fp_device_get_nr_enroll_stages (priv->dev));
		else
			g_value_set_int (value, -1);
		break;
	case FPRINT_DEVICE_SCAN_TYPE: {
		const char *type;

		if (fp_device_get_scan_type (priv->dev) == FP_SCAN_TYPE_PRESS)
			type = "press";
		else
			type = "swipe";

		g_value_set_static_string (value, type);
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
		break;
	}
}

static void fprint_device_class_init(FprintDeviceClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	GParamSpec *pspec;

	gobject_class->dispose = fprint_device_dispose;
	gobject_class->finalize = fprint_device_finalize;
	gobject_class->set_property = fprint_device_set_property;
	gobject_class->get_property = fprint_device_get_property;

	pspec = g_param_spec_object("dev", "Device",
				     "Set device construction property",
				     FP_TYPE_DEVICE,
				     G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE | G_PARAM_READABLE);
	g_object_class_install_property(gobject_class,
					FPRINT_DEVICE_CONSTRUCT_DEV, pspec);

	pspec = g_param_spec_boolean("in-use", "In use",
				     "Whether the device is currently in use", FALSE,
				     G_PARAM_READABLE);
	g_object_class_install_property(gobject_class,
					FPRINT_DEVICE_IN_USE, pspec);

	g_object_class_override_property (gobject_class,
					  FPRINT_DEVICE_NAME,
					  "name");

	g_object_class_override_property (gobject_class,
					  FPRINT_DEVICE_SCAN_TYPE,
					  "scan-type");

	g_object_class_override_property (gobject_class,
					  FPRINT_DEVICE_NUM_ENROLL,
					  "num-enroll-stages");

	signals[SIGNAL_VERIFY_STATUS] =
		g_signal_lookup ("verify-status", FPRINT_TYPE_DEVICE);
	signals[SIGNAL_ENROLL_STATUS] =
		g_signal_lookup ("enroll-status", FPRINT_TYPE_DEVICE);
	signals[SIGNAL_VERIFY_FINGER_SELECTED] =
		g_signal_lookup ("verify-finger-selected", FPRINT_TYPE_DEVICE);

	quark_auth_user = g_quark_from_static_string ("authorized-user");
}

static void _unwatch_name (gpointer id)
{
	g_bus_unwatch_name (GPOINTER_TO_INT (id));
}

static void fprint_device_init(FprintDevice *device)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private(device);
	priv->id = ++last_id;

	/* Setup PolicyKit */
	priv->auth = polkit_authority_get_sync (NULL, NULL);
	priv->clients = g_hash_table_new_full (g_str_hash,
					       g_str_equal,
					       g_free,
					       _unwatch_name);

	g_signal_connect (device, "g-authorize-method",
			  G_CALLBACK (action_authorization_handler),
			  NULL);
}

FprintDevice *fprint_device_new(FpDevice *dev)
{
	return g_object_new(FPRINT_TYPE_DEVICE, "dev", dev, NULL);
}

guint32 _fprint_device_get_id(FprintDevice *rdev)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);

	return priv->id;
}

static const char *
finger_num_to_name (int finger_num)
{
	if (finger_num == -1)
		return "any";
	if (!FP_FINGER_IS_VALID (finger_num))
		return NULL;
	return FINGERS_NAMES[finger_num];
}

static int
finger_name_to_num (const char *finger_name)
{
	guint i;

	if (finger_name == NULL || *finger_name == '\0' || g_str_equal (finger_name, "any"))
		return -1;

	for (i = FP_FINGER_FIRST; i <= FP_FINGER_LAST; i++) {
		if (g_str_equal (finger_name, FINGERS_NAMES[i]))
			return i;
	}

	/* Invalid, let's try that */
	return -1;
}

static const char *
verify_result_to_name (gboolean match, GError *error)
{
	if (!error) {
		if (match)
			return "verify-match";
		else
			return "verify-no-match";
	}

	if (error->domain == FP_DEVICE_RETRY) {
		switch (error->code) {
			case FP_DEVICE_RETRY_TOO_SHORT:
				return "verify-swipe-too-short";
			case FP_DEVICE_RETRY_CENTER_FINGER:
				return "verify-finger-not-centered";
			case FP_DEVICE_RETRY_REMOVE_FINGER:
				return "verify-remove-and-retry";
			default:
				return "verify-retry-scan";
		}
	} else {
		/* Which errors should be mapped to disconnection?
		 * Are drivers/libfprint/fprintd really in agreement here?
		 */
		if (g_error_matches (error, FP_DEVICE_ERROR, FP_DEVICE_ERROR_PROTO))
			return "verify-disconnected";
		else if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			return "verify-no-match";

		return "verify-unknown-error";
	}
}

static const char *
enroll_result_to_name (gboolean completed, gboolean enrolled, GError *error)
{
	if (!error) {
		if (!completed)
			return "enroll-stage-passed";
		else if (enrolled)
			return "enroll-completed";
		else
			return "enroll-failed";
	}

	if (error->domain == FP_DEVICE_RETRY) {
		switch (error->code) {
			case FP_DEVICE_RETRY_TOO_SHORT:
				return "enroll-swipe-too-short";
			case FP_DEVICE_RETRY_CENTER_FINGER:
				return "enroll-finger-not-centered";
			case FP_DEVICE_RETRY_REMOVE_FINGER:
				return "enroll-remove-and-retry";
			default:
				return "enroll-retry-scan";
		}
	} else {
		/* Which errors should be mapped to disconnection?
		 * Are drivers/libfprint/fprintd really in agreement here?
		 */
		if (g_error_matches (error, FP_DEVICE_ERROR, FP_DEVICE_ERROR_PROTO))
			return "enroll-disconnected";
		else if (g_error_matches (error, FP_DEVICE_ERROR, FP_DEVICE_ERROR_DATA_FULL))
			return "enroll-data-full";
		else if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			return "enroll-failed";

		return "enroll-unknown-error";
	}
}

static gboolean
_fprint_device_check_claimed (FprintDevice *rdev,
			      GDBusMethodInvocation *invocation,
			      FprintDeviceClaimState requested_state,
			      GError **error)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(SessionData) session = NULL;
	const char *sender;
	gboolean retval;

	if (requested_state == STATE_IGNORED)
		return TRUE;

	session = session_data_get (priv);

	if (requested_state == STATE_UNCLAIMED) {
		/* Is it already claimed? */
		if (!session) {
			return TRUE;
		}

		g_set_error(error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
			    "Device was already claimed");
		return FALSE;
	}

	g_assert (requested_state == STATE_CLAIMED);

	/* The device wasn't claimed, exit */
	if (session == NULL) {
		g_set_error (error, FPRINT_ERROR, FPRINT_ERROR_CLAIM_DEVICE,
			     _("Device was not claimed before use"));
		return FALSE;
	}

	sender = g_dbus_method_invocation_get_sender (invocation);
	retval = g_str_equal (sender, session->sender);

	if (retval == FALSE || session->invocation != NULL) {
		g_set_error (error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
			     _("Device already in use by another user"));
	}

	return retval;
}

static gboolean
_fprint_device_check_polkit_for_action (FprintDevice *rdev,
				        GDBusMethodInvocation *invocation,
				        const char *action,
				        GError **error)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	const char *sender;
	g_autoptr(GError) _error = NULL;
	g_autoptr(PolkitAuthorizationResult) result = NULL;
	g_autoptr(PolkitSubject) subject = NULL;

	/* Check that caller is privileged */
	sender = g_dbus_method_invocation_get_sender (invocation);
	subject = polkit_system_bus_name_new (sender);

	result = polkit_authority_check_authorization_sync (priv->auth,
                                                            subject,
                                                            action,
							    NULL,
                                                            POLKIT_CHECK_AUTHORIZATION_FLAGS_ALLOW_USER_INTERACTION,
					                    NULL, &_error);
	if (result == NULL) {
		g_set_error (error, FPRINT_ERROR,
			     FPRINT_ERROR_PERMISSION_DENIED,
			     "Not Authorized: %s", _error->message);
		return FALSE;
	}

	if (!polkit_authorization_result_get_is_authorized (result)) {
		g_set_error (error, FPRINT_ERROR,
			     FPRINT_ERROR_PERMISSION_DENIED,
			     "Not Authorized: %s", action);
		return FALSE;
	}

	return TRUE;
}

static gboolean
fprint_device_check_polkit_for_permissions (FprintDevice *rdev,
					    GDBusMethodInvocation *invocation,
					    FprintDevicePermission permissions,
					    GError **error)
{
	g_autoptr(GFlagsClass) permission_flags = NULL;
	unsigned i;

	if (permissions == FPRINT_DEVICE_PERMISSION_NONE)
		return TRUE;

	permission_flags = g_type_class_ref (FPRINT_TYPE_DEVICE_PERMISSION);

	for (i = 0; i < permission_flags->n_values; ++i) {
		GFlagsValue *value = &permission_flags->values[i];
		const char *action;

		if (!(value->value & permissions)) {
			continue;
		}

		action = value->value_nick;
		g_debug ("Getting authorization to perform Polkit action %s",
			 action);

		g_clear_error (error);
		if (_fprint_device_check_polkit_for_action (rdev, invocation,
							    action, error)) {
			return TRUE;
		}
	}

	g_assert (!error || *error);
	return FALSE;
}

static char *
_fprint_device_check_for_username (FprintDevice *rdev,
				   GDBusMethodInvocation *invocation,
				   const char *username,
				   GError **error)
{
	g_autoptr(GVariant) ret = NULL;
	g_autoptr(GError) err = NULL;
	GDBusConnection *connection;
	const char *sender;
	struct passwd *user;
	guint32 uid;

	/* Get details about the current sender, and username/uid */
	connection = g_dbus_method_invocation_get_connection (invocation);
	sender = g_dbus_method_invocation_get_sender (invocation);

	ret = g_dbus_connection_call_sync (connection,
					   "org.freedesktop.DBus",
					   "/org/freedesktop/DBus",
					   "org.freedesktop.DBus",
					   "GetConnectionUnixUser",
					   g_variant_new ("(s)", sender),
					   NULL, G_DBUS_CALL_FLAGS_NONE, -1,
					   NULL, &err);

	if (!ret) {
		g_autoptr(GError) e = NULL;

		g_set_error (error, FPRINT_ERROR, FPRINT_ERROR_INTERNAL,
			     "Could not get conection unix user ID: %s",
			     err->message);
		return NULL;
	}

	g_variant_get (ret, "(u)", &uid);
	user = getpwuid (uid);
	if (user == NULL) {
		g_set_error(error, FPRINT_ERROR, FPRINT_ERROR_INTERNAL,
			    "Failed to get information about user UID %u", uid);
		return NULL;
	}

	/* The current user is usually allowed to access their
	 * own data, this should be followed by PolicyKit checks
	 * anyway */
	if (username == NULL || *username == '\0' || g_str_equal (username, user->pw_name)) {
		return g_strdup (user->pw_name);
	}

	/* If we're not allowed to set a different username,
	 * then fail */
	if (!fprint_device_check_polkit_for_permissions (rdev, invocation,
							 FPRINT_DEVICE_PERMISSION_SETUSERNAME,
							 error)) {
		return NULL;
	}

	return g_strdup (username);
}

static void
_fprint_device_client_vanished (GDBusConnection *connection,
				const char *name,
				FprintDevice *rdev)
{
	g_autoptr(GError) error = NULL;
	g_autoptr(SessionData) session = NULL;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);

	session = session_data_get (priv);

	/* Was that the client that claimed the device? */
	if (session != NULL &&
	    g_strcmp0 (session->sender, name) == 0) {
		while (priv->current_action != ACTION_NONE) {
			/* OPEN/CLOSE are not cancellable, we just need to wait */
			if (priv->current_cancellable)
				g_cancellable_cancel (priv->current_cancellable);

			g_main_context_iteration (NULL, TRUE);
		}

		/* The session may have disappeared at this point if the device
		 * was already closing. */
		g_clear_pointer (&session, session_data_unref);
		session = session_data_get (priv);
		if (session && !fp_device_close_sync (priv->dev, NULL, &error))
			g_critical ("Error closing device after disconnect: %s", error->message);

		session_data_set_new (priv, NULL, NULL);
	}
	g_hash_table_remove (priv->clients, name);

	if (g_hash_table_size (priv->clients) == 0) {
		g_object_notify (G_OBJECT (rdev), "in-use");
	}
}

static void
_fprint_device_add_client (FprintDevice *rdev, const char *sender)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	guint id;

	id = GPOINTER_TO_UINT (g_hash_table_lookup (priv->clients, sender));
	if (id == 0) {
		id = g_bus_watch_name (G_BUS_TYPE_SYSTEM,
				       sender,
				       G_BUS_NAME_WATCHER_FLAGS_NONE,
				       NULL,
				       (GBusNameVanishedCallback) _fprint_device_client_vanished,
				       rdev,
				       NULL);
		g_hash_table_insert (priv->clients, g_strdup (sender), GUINT_TO_POINTER(id));
		g_object_notify (G_OBJECT (rdev), "in-use");
	}
}

static void dev_open_cb(FpDevice *dev, GAsyncResult *res, void *user_data)
{
	g_autoptr(GError) error = NULL;
	FprintDevice *rdev = user_data;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(SessionData) session = NULL;
	g_autoptr(GDBusMethodInvocation) invocation = NULL;

	session = session_data_get (priv);
	invocation = g_steal_pointer (&session->invocation);

	priv->current_action = ACTION_NONE;
	if (!fp_device_open_finish (dev, res, &error)) {
		g_autoptr(GError) dbus_error = NULL;

		dbus_error = g_error_new (FPRINT_ERROR,
		                          FPRINT_ERROR_INTERNAL,
		                          "Open failed with error: %s", error->message);
		g_dbus_method_invocation_return_gerror (invocation, dbus_error);
		session_data_set_new (priv, NULL, NULL);
		return;
	}

	g_debug("claimed device %d", priv->id);

	fprint_dbus_device_complete_claim (FPRINT_DBUS_DEVICE (rdev),
					   invocation);
}

static gboolean
fprintd_device_authorize_user (FprintDevice *rdev,
			       GDBusMethodInvocation *invocation,
			       GError **error)
{
	GVariant *params = NULL;
	const char *username = NULL;
	g_autofree char *user = NULL;

	params = g_dbus_method_invocation_get_parameters (invocation);
	g_assert (g_variant_n_children (params) == 1);
	g_variant_get (params, "(&s)", &username);
	g_assert (username);

	user = _fprint_device_check_for_username (rdev,
						  invocation,
						  username,
						  error);
	if (user == NULL) {
		return FALSE;
	}

	/* We keep the user attached to the invocation as it may not be the same
	 * of the requested one, in case an empty one was passed.
	 * Given that now we may have multiple cuncurrent requests, it wouldn't
	 * be safe to add another member to the priv, as it would need even more
	 * multi-thread checks around, and over-complicate things.
	 */
	g_object_set_qdata_full (G_OBJECT (invocation), quark_auth_user,
				 g_steal_pointer (&user), g_free);

	return TRUE;
}

static gboolean fprint_device_claim (FprintDBusDevice *dbus_dev,
				     GDBusMethodInvocation *invocation,
				     const char *username)
{
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(SessionData) session = NULL;
	g_autoptr(GError) error = NULL;
	char *sender, *user;

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_UNCLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	user = g_object_steal_qdata (G_OBJECT (invocation), quark_auth_user);
	g_assert (user);

	sender = g_strdup (g_dbus_method_invocation_get_sender (invocation));
	_fprint_device_add_client (rdev, sender);

	session = session_data_set_new (priv, g_steal_pointer (&sender), g_steal_pointer(&user));
	session->invocation = g_object_ref (invocation);
	username = g_steal_pointer (&user);
	sender = g_steal_pointer (&sender);

	g_debug ("user '%s' claiming the device: %d", session->username, priv->id);

	priv->current_action = ACTION_OPEN;
	fp_device_open (priv->dev, NULL, (GAsyncReadyCallback) dev_open_cb, rdev);

	return TRUE;
}

static void dev_close_cb(FpDevice *dev, GAsyncResult *res, void *user_data)
{
	g_autoptr(GError) error = NULL;
	FprintDevice *rdev = user_data;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(SessionData) session = NULL;
	g_autoptr(GDBusMethodInvocation) invocation = NULL;

	session = session_data_get (priv);
	session_data_set_new (priv, NULL, NULL);
	invocation = g_steal_pointer (&session->invocation);

	priv->current_action = ACTION_NONE;
	if (!fp_device_close_finish (dev, res, &error)) {
		g_autoptr(GError) dbus_error = NULL;

		dbus_error = g_error_new (FPRINT_ERROR,
		                          FPRINT_ERROR_INTERNAL,
		                          "Release failed with error: %s", error->message);
		g_dbus_method_invocation_return_gerror (invocation, dbus_error);
		return;
	}

	g_debug("released device %d", priv->id);

	fprint_dbus_device_complete_release  (FPRINT_DBUS_DEVICE (rdev),
					      invocation);
}

static gboolean fprint_device_release (FprintDBusDevice *dbus_dev,
				       GDBusMethodInvocation *invocation)
{
	g_autoptr(GError) error = NULL;
	g_autoptr(SessionData) session = NULL;
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (priv->current_cancellable) {
		if (priv->current_action == ACTION_ENROLL) {
			g_warning("Enrollment was in progress, stopping it");
		} else if (priv->current_action == ACTION_IDENTIFY ||
			   priv->current_action == ACTION_VERIFY) {
			g_warning("Verification was in progress, stopping it");
		}

		g_cancellable_cancel (priv->current_cancellable);
		while (priv->current_action != ACTION_NONE)
			g_main_context_iteration (NULL, TRUE);
	}

	session = session_data_get (priv);
	session->invocation = g_object_ref (invocation);

	priv->current_action = ACTION_CLOSE;
	fp_device_close (priv->dev, NULL, (GAsyncReadyCallback) dev_close_cb, rdev);

	return TRUE;
}

static void report_verify_status (FprintDevice *rdev,
				  gboolean match,
				  GError *error)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private (rdev);
	const char *result = verify_result_to_name (match, error);
	g_autoptr(SessionData) session = NULL;
	gboolean done;

	done = (error == NULL || error->domain != FP_DEVICE_RETRY);

	session = session_data_get (priv);

	if (done && session->verify_status_reported) {
		/* It is completely fine for cancellation to occur after a
		 * result has been reported. */
		if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			g_warning ("Verify status already reported. Ignoring %s", result);
		return;
	}

	g_debug ("report_verify_status: result %s", result);
	g_signal_emit (rdev, signals[SIGNAL_VERIFY_STATUS], 0, result, done);

	if (done)
		session->verify_status_reported = TRUE;
}

static void match_cb (FpDevice *device,
		      FpPrint *match,
		      FpPrint *print,
		      gpointer user_data,
		      GError *error)
{
	FprintDevice *rdev = user_data;
	FprintDevicePrivate *priv = fprint_device_get_instance_private (rdev);
	gboolean matched;
	gboolean cancelled;

	g_assert_true (error == NULL || error->domain == FP_DEVICE_RETRY);

	cancelled = g_cancellable_is_cancelled (priv->current_cancellable);
	matched = match != NULL && cancelled == FALSE;

	/* No-match is reported only after the operation completes.
	 * This avoids problems when the operation is immediately restarted. */
	report_verify_status (rdev, matched, error);
}

static void verify_cb(FpDevice *dev, GAsyncResult *res, void *user_data)
{
	g_autoptr(GError) error = NULL;
	g_autoptr(SessionData) session = NULL;
	FprintDevice *rdev = user_data;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	FprintDBusDevice *dbus_dev = FPRINT_DBUS_DEVICE (rdev);
	gboolean success;
	const char *name;
	gboolean match;

	success = fp_device_verify_finish (dev, res, &match, NULL, &error);
	g_assert (!!success == !error);
	name = verify_result_to_name (match, error);

	session = session_data_get (priv);

	g_debug("verify_cb: result %s", name);

	/* Automatically restart the operation for retry failures */
	if (error && error->domain == FP_DEVICE_RETRY) {
		fp_device_verify (priv->dev,
				  priv->verify_data,
				  priv->current_cancellable,
				  match_cb, rdev, NULL,
				  (GAsyncReadyCallback) verify_cb,
				  rdev);
	} else {
		g_clear_object (&priv->verify_data);

		if (error) {
			report_verify_status (rdev, FALSE, error);

			if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
				g_warning ("Device reported an error during verify: %s",
					   error->message);
			}
		}

		/* Return the cancellation or reset action right away if vanished. */
		if (priv->current_cancel_invocation) {
			fprint_dbus_device_complete_verify_stop (dbus_dev,
				g_steal_pointer (&priv->current_cancel_invocation));
			priv->current_action = ACTION_NONE;
			session->verify_status_reported = FALSE;
		} else if (g_cancellable_is_cancelled (priv->current_cancellable)) {
			priv->current_action = ACTION_NONE;
			session->verify_status_reported = FALSE;
		}

		g_clear_object (&priv->current_cancellable);
	}
}

static void identify_cb(FpDevice *dev, GAsyncResult *res, void *user_data)
{
	g_autoptr(GError) error = NULL;
	g_autoptr(FpPrint) match = NULL;
	FprintDevice *rdev = user_data;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	FprintDBusDevice *dbus_dev = FPRINT_DBUS_DEVICE (rdev);
	const char *name;
	gboolean success;

	success = fp_device_identify_finish (dev, res, &match, NULL, &error);
	g_assert (!!success == !error);
	name = verify_result_to_name (match != NULL, error);

	g_debug("identify_cb: result %s", name);

	/* Automatically restart the operation for retry failures */
	if (error && error->domain == FP_DEVICE_RETRY) {
		fp_device_identify (priv->dev,
				    priv->identify_data,
				    priv->current_cancellable,
				    match_cb, rdev, NULL,
				    (GAsyncReadyCallback) identify_cb,
				    rdev);
	} else {
		g_clear_pointer (&priv->identify_data, g_ptr_array_unref);

		if (error) {
			report_verify_status (rdev, FALSE, error);

			if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
				g_warning ("Device reported an error during identify: %s",
					   error->message);
			}
		}

		/* Return the cancellation or reset action right away if vanished. */
		if (priv->current_cancel_invocation) {
			fprint_dbus_device_complete_verify_stop (dbus_dev,
				g_steal_pointer (&priv->current_cancel_invocation));
			priv->current_action = ACTION_NONE;
		} else if (g_cancellable_is_cancelled (priv->current_cancellable)) {
			g_autoptr(SessionData) session = NULL;
			session = session_data_get (priv);
			priv->current_action = ACTION_NONE;
			session->verify_status_reported = FALSE;
		}

		g_clear_object (&priv->current_cancellable);
	}
}

static gboolean fprint_device_verify_start (FprintDBusDevice *dbus_dev,
					    GDBusMethodInvocation *invocation,
					    const char *finger_name)
{
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(GPtrArray) gallery = NULL;
	g_autoptr(FpPrint) print = NULL;
	g_autoptr(SessionData) session = NULL;
	g_autoptr(GError) error = NULL;
	guint finger_num = finger_name_to_num (finger_name);

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	session = session_data_get (priv);

	if (priv->current_action != ACTION_NONE) {
		if (priv->current_action == ACTION_ENROLL) {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
				    "Enrollment in progress");
		} else {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
				    "Verification already in progress");
		}
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (finger_num == -1) {
		g_autoptr(GSList) prints = NULL;

		prints = store.discover_prints(priv->dev, session->username);
		if (prints == NULL) {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_NO_ENROLLED_PRINTS,
				    "No fingerprints enrolled");
			g_dbus_method_invocation_return_gerror (invocation, error);
			return TRUE;
		}
		if (fp_device_supports_identify (priv->dev)) {
			GSList *l;

			gallery = g_ptr_array_new_with_free_func (g_object_unref);

			for (l = prints; l != NULL; l = l->next) {
				g_debug ("adding finger %d to the gallery", GPOINTER_TO_INT (l->data));
				store.print_data_load(priv->dev, GPOINTER_TO_INT (l->data),
						      session->username, &print);

				if (print)
					g_ptr_array_add (gallery, g_steal_pointer (&print));
			}
		} else {
			finger_num = GPOINTER_TO_INT (prints->data);
		}
	}

	if (fp_device_supports_identify (priv->dev) && finger_num == -1) {
		if (gallery->len == 0) {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_NO_ENROLLED_PRINTS,
				    "No fingerprints on that device");
			g_dbus_method_invocation_return_gerror (invocation, error);
			return TRUE;
		}
		priv->current_action = ACTION_IDENTIFY;

		g_debug ("start identification device %d", priv->id);
		priv->current_cancellable = g_cancellable_new ();
		priv->identify_data = g_ptr_array_ref (gallery);
		fp_device_identify (priv->dev, gallery, priv->current_cancellable,
				    match_cb, rdev, NULL,
		                    (GAsyncReadyCallback) identify_cb, rdev);
	} else {
		priv->current_action = ACTION_VERIFY;

		g_debug("start verification device %d finger %d", priv->id, finger_num);

		store.print_data_load(priv->dev, finger_num,
				      session->username, &print);

		if (!print) {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_NO_ENROLLED_PRINTS,
				    "No such print %d", finger_num);
			g_dbus_method_invocation_return_gerror (invocation,
								error);
			return TRUE;
		}

		priv->current_cancellable = g_cancellable_new ();
		priv->verify_data = g_object_ref (print);
		fp_device_verify (priv->dev, print, priv->current_cancellable,
				  match_cb, rdev, NULL,
		                  (GAsyncReadyCallback) verify_cb, rdev);
	}

	/* Emit VerifyFingerSelected telling the front-end which finger
	 * we selected for auth */
	g_signal_emit(rdev, signals[SIGNAL_VERIFY_FINGER_SELECTED],
		      0, finger_num_to_name (finger_num));

	fprint_dbus_device_complete_verify_start  (dbus_dev, invocation);

	return TRUE;
}

static gboolean fprint_device_verify_stop (FprintDBusDevice *dbus_dev,
					   GDBusMethodInvocation *invocation)
{
	g_autoptr(SessionData) session = NULL;
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(GError) error = NULL;

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (priv->current_action == ACTION_NONE) {
		g_dbus_method_invocation_return_error_literal (invocation,
							       FPRINT_ERROR,
							       FPRINT_ERROR_NO_ACTION_IN_PROGRESS,
							       "No verification in progress");
		return TRUE;
	} else if (priv->current_action == ACTION_ENROLL) {
		g_dbus_method_invocation_return_error_literal (invocation,
							       FPRINT_ERROR,
							       FPRINT_ERROR_ALREADY_IN_USE,
							       "Enrollment in progress");
		return TRUE;
	}

	if (priv->current_cancellable) {
		/* We return only when the action was cancelled */
		g_cancellable_cancel (priv->current_cancellable);
		priv->current_cancel_invocation = invocation;
	} else {
		fprint_dbus_device_complete_verify_stop (dbus_dev, invocation);
		priv->current_action = ACTION_NONE;

		session = session_data_get (priv);
		session->verify_status_reported = FALSE;
	}

	return TRUE;
}

static void enroll_progress_cb(FpDevice *dev,
                               gint      completed_stages,
                               FpPrint  *print,
                               gpointer  user_data,
                               GError   *error)
{
	FprintDevice *rdev = user_data;
	const char *name = enroll_result_to_name (FALSE, FALSE, error);

	g_debug("enroll_stage_cb: result %s", name);

	if (completed_stages < fp_device_get_nr_enroll_stages (dev))
		g_signal_emit(rdev, signals[SIGNAL_ENROLL_STATUS], 0, name, FALSE);
}

static gboolean try_delete_print(FprintDevice *rdev)
{
	g_autoptr(GError) error = NULL;
	g_autoptr(GPtrArray) device_prints = NULL;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	GSList *users, *user;

	device_prints = fp_device_list_prints_sync (priv->dev, NULL, &error);
	if (!device_prints) {
		g_warning ("Failed to query prints: %s", error->message);
		return FALSE;
	}

	g_debug ("Device has %d prints stored", device_prints->len);

	users = store.discover_users();

	for (user = users; user; user = user->next) {
		const char *username = user->data;
		g_autoptr(GSList) fingers = NULL;
		GSList *finger;

		fingers = store.discover_prints (priv->dev, username);

		for (finger = fingers; finger; finger = finger->next) {
			g_autoptr(FpPrint) print = NULL;
			guint index;

			store.print_data_load (priv->dev,
			                       GPOINTER_TO_INT (fingers->data),
			                       username,
			                       &print);

			if (!print)
				continue;

			if (!g_ptr_array_find_with_equal_func (device_prints,
			                                       print,
			                                       (GEqualFunc) fp_print_equal,
			                                       &index))
				continue;

			/* Found an equal print, remove it */
			g_ptr_array_remove_index (device_prints, index);
		}
	}

	g_slist_free_full (users, g_free);

	g_debug ("Device has %d prints stored that we do not need", device_prints->len);
	if (device_prints->len == 0)
		return FALSE;

	/* Just delete the first print in the list at this point.
	 * We could be smarter and fetch some more metadata. */
	fp_device_delete_print_sync (priv->dev,
				     g_ptr_array_index (device_prints, 0),
				     NULL,
				     &error);

	if (error) {
		g_warning ("Failed to garbage collect a print: %s", error->message);
		return FALSE;
	}

	return TRUE;
}

#if !GLIB_CHECK_VERSION (2, 63, 3)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GDate, g_date_free);
#endif

static FpPrint*
fprint_device_create_enroll_template(FprintDevice *rdev, gint finger_num)
{
	g_autoptr(SessionData) session = NULL;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(GDateTime) datetime = NULL;
	g_autoptr(GDate) date = NULL;
	FpPrint *template = NULL;
	gint year, month, day;

	session = session_data_get (priv);

	template = fp_print_new (priv->dev);
	fp_print_set_finger (template, finger_num);
	fp_print_set_username (template, session->username);
	datetime = g_date_time_new_now_local ();
	g_date_time_get_ymd (datetime, &year, &month, &day);
	date = g_date_new_dmy (day, month, year);
	fp_print_set_enroll_date (template, date);

	return template;
}

static void enroll_cb(FpDevice *dev, GAsyncResult *res, void *user_data)
{
	g_autoptr(GError) error = NULL;
	FprintDevice *rdev = user_data;
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	FprintDBusDevice *dbus_dev = FPRINT_DBUS_DEVICE (rdev);
	g_autoptr(FpPrint) print = NULL;
	const char *name;

	print = fp_device_enroll_finish (dev, res, &error);

	/* We need to special case the issue where the on device storage
	 * is completely full. In that case, we check whether we can delete
	 * a print that is not coming from us; assuming it is from an old
	 * installation.
	 * We do this synchronously, which is not great but should be good
	 * enough. */
	if (g_error_matches (error, FP_DEVICE_ERROR, FP_DEVICE_ERROR_DATA_FULL)) {
		g_debug ("Device storage is full, trying to garbage collect old prints");
		if (try_delete_print (rdev)) {
			/* Success? Then restart the operation */
			fp_device_enroll (priv->dev,
			                  fprint_device_create_enroll_template (rdev, priv->enroll_data),
			                  priv->current_cancellable,
			                  enroll_progress_cb,
			                  rdev,
			                  NULL,
			                  (GAsyncReadyCallback) enroll_cb,
			                  rdev);
			return;
		}
	}

	name = enroll_result_to_name (TRUE, print != NULL, error);

	g_debug ("enroll_cb: result %s", name);

	if (print) {
		int r;
		r = store.print_data_save(print);
		if (r < 0)
			name = "enroll-failed";
	}

	g_signal_emit(rdev, signals[SIGNAL_ENROLL_STATUS], 0, name, TRUE);

	if (error && !g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
		g_warning ("Device reported an error during enroll: %s", error->message);

	/* Return the cancellation or reset action right away if vanished. */
	if (priv->current_cancel_invocation) {
		fprint_dbus_device_complete_enroll_stop (dbus_dev,
			g_steal_pointer (&priv->current_cancel_invocation));
		priv->current_action = ACTION_NONE;
	} else if (g_cancellable_is_cancelled (priv->current_cancellable)) {
		priv->current_action = ACTION_NONE;
	}
	g_clear_object (&priv->current_cancellable);
}


static gboolean fprint_device_enroll_start (FprintDBusDevice *dbus_dev,
					    GDBusMethodInvocation *invocation,
					    const char *finger_name)
{
	g_autoptr(GError) error = NULL;
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	int finger_num = finger_name_to_num (finger_name);

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (finger_num == -1) {
		g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_INVALID_FINGERNAME,
			    "Invalid finger name");
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (priv->current_action != ACTION_NONE) {
		if (priv->current_action == ACTION_ENROLL) {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
				    "Enrollment already in progress");
		} else {
			g_set_error(&error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
				    "Verification in progress");
		}
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	g_debug("start enrollment device %d finger %d", priv->id, finger_num);

	priv->current_cancellable = g_cancellable_new ();
	priv->enroll_data = finger_num;
	fp_device_enroll (priv->dev,
	                  fprint_device_create_enroll_template (rdev, priv->enroll_data),
	                  priv->current_cancellable,
	                  enroll_progress_cb,
	                  rdev,
	                  NULL,
	                  (GAsyncReadyCallback) enroll_cb,
	                  rdev);

	priv->current_action = ACTION_ENROLL;

	fprint_dbus_device_complete_enroll_start (dbus_dev, invocation);

	return TRUE;
}

static gboolean fprint_device_enroll_stop (FprintDBusDevice *dbus_dev,
					   GDBusMethodInvocation *invocation)
{
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(GError) error = NULL;

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (priv->current_action != ACTION_ENROLL) {
		if (priv->current_action == ACTION_NONE) {
			g_set_error (&error, FPRINT_ERROR, FPRINT_ERROR_NO_ACTION_IN_PROGRESS,
				     "No enrollment in progress");
		} else if (priv->current_action == ACTION_VERIFY) {
			g_set_error (&error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
				     "Verification in progress");
		} else if (priv->current_action == ACTION_IDENTIFY) {
			g_set_error (&error, FPRINT_ERROR, FPRINT_ERROR_ALREADY_IN_USE,
				     "Identification in progress");
		} else
			g_assert_not_reached ();
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	if (priv->current_cancellable) {
		/* We return only when the action was cancelled */
		g_cancellable_cancel (priv->current_cancellable);
		priv->current_cancel_invocation = invocation;
	} else {
		fprint_dbus_device_complete_enroll_stop (dbus_dev, invocation);
		priv->current_action = ACTION_NONE;
	}

	return TRUE;
}
static gboolean fprint_device_list_enrolled_fingers (FprintDBusDevice *dbus_dev,
						     GDBusMethodInvocation *invocation,
						     const char *username)
{
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr (GPtrArray) ret = NULL;
	g_autoptr(GSList) prints = NULL;
	GSList *item;
	const char *sender;
	const char *user;

	sender = g_dbus_method_invocation_get_sender (invocation);
	_fprint_device_add_client (rdev, sender);

	user = g_object_get_qdata (G_OBJECT (invocation), quark_auth_user);
	g_assert (user);
	prints = store.discover_prints(priv->dev, user);

	if (!prints) {
		g_dbus_method_invocation_return_error_literal (invocation,
							       FPRINT_ERROR,
							       FPRINT_ERROR_NO_ENROLLED_PRINTS,
							       "Failed to discover prints");
		return TRUE;
	}

	ret = g_ptr_array_new ();
	for (item = prints; item; item = item->next) {
		int finger_num = GPOINTER_TO_INT (item->data);
		g_ptr_array_add (ret, (char *) finger_num_to_name (finger_num));
	}
	g_ptr_array_add (ret, NULL);

	fprint_dbus_device_complete_list_enrolled_fingers  (dbus_dev,
		invocation, (const gchar *const *) ret->pdata);

	return TRUE;
}

static void delete_enrolled_fingers(FprintDevice *rdev, const char *user)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	guint i;

	g_debug ("Deleting enrolled fingers for user %s", user);

	/* First try deleting the print from the device, we don't consider it
	 * fatal if this does not work. */
	if (fp_device_has_storage (priv->dev)) {
		g_autoptr(GSList) prints = NULL;
		GSList *l;

		prints = store.discover_prints(priv->dev, user);

		for (l = prints; l != NULL; l = l->next) {
			g_autoptr(FpPrint) print = NULL;

			store.print_data_load(priv->dev,
			                      GPOINTER_TO_INT (l->data),
			                      user,
			                      &print);

			if (print) {
				g_autoptr(GError) error = NULL;

				if (!fp_device_delete_print_sync (priv->dev, print, NULL, &error)) {
					g_warning ("Error deleting print from device: %s", error->message);
					g_warning ("This might indicate an issue in the libfprint driver or in the fingerprint device.");
				}
			}
		}
	}

	for (i = FP_FINGER_FIRST; i <= FP_FINGER_LAST; i++) {
		store.print_data_delete(priv->dev, i, user);
	}
}

#ifdef __linux__
static void log_offending_client_cb (GObject *object,
				     GAsyncResult *res,
				     gpointer user_data)
{
	GDBusConnection *connection = G_DBUS_CONNECTION (object);
	g_autoptr(GVariant) ret = NULL;
	g_autofree char *path = NULL;
	g_autofree char *content = NULL;
	guint pid = 0;

	ret = g_dbus_connection_call_finish (connection, res, NULL);

	if (!ret)
		return;

	g_variant_get (ret, "(u)", &pid);
	path = g_strdup_printf ("/proc/%u/comm", pid);
	if (g_file_get_contents (path, &content, NULL, NULL)) {
		g_strchomp (content);
		g_warning ("Offending API user is %s", content);
	}
}

static void log_offending_client (GDBusMethodInvocation *invocation)
{
	const char *sender;
	GDBusConnection *connection;

	connection = g_dbus_method_invocation_get_connection (invocation);
	sender = g_dbus_method_invocation_get_sender (invocation);

	g_dbus_connection_call (connection,
			        "org.freedesktop.DBus",
			        "/org/freedesktop/DBus",
			        "org.freedesktop.DBus",
			        "GetConnectionUnixProcessID",
			        g_variant_new ("(s)", sender),
			        NULL, G_DBUS_CALL_FLAGS_NONE,
			        -1, NULL, log_offending_client_cb, NULL);
}
#endif

static gboolean fprint_device_delete_enrolled_fingers (FprintDBusDevice *dbus_dev,
						       GDBusMethodInvocation *invocation,
						       const char *username)
{
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(GError) error = NULL;
	g_autofree char *user = NULL;
	const char *sender;
	gboolean opened;

	g_warning ("The API user should be updated to use DeleteEnrolledFingers2 method!");
#ifdef __linux__
	log_offending_client (invocation);
#endif

	user = g_object_steal_qdata (G_OBJECT (invocation), quark_auth_user);
	g_assert (user);

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED,
					   &error)) {
		/* Return error for anything but FPRINT_ERROR_CLAIM_DEVICE */
		if (!g_error_matches (error, FPRINT_ERROR, FPRINT_ERROR_CLAIM_DEVICE)) {
			g_dbus_method_invocation_return_gerror (invocation,
								error);
			return TRUE;
		}

		opened = FALSE;
	} else {
		opened = TRUE;
	}

	sender = g_dbus_method_invocation_get_sender (invocation);
	_fprint_device_add_client (rdev, sender);

	if (!opened && fp_device_has_storage (priv->dev))
		fp_device_open_sync (priv->dev, NULL, NULL);

	delete_enrolled_fingers (rdev, user);

	if (!opened && fp_device_has_storage (priv->dev))
		fp_device_close_sync (priv->dev, NULL, NULL);

	fprint_dbus_device_complete_delete_enrolled_fingers (dbus_dev,
							     invocation);
	return TRUE;
}

static gboolean fprint_device_delete_enrolled_fingers2 (FprintDBusDevice *dbus_dev,
							GDBusMethodInvocation *invocation)
{
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private(rdev);
	g_autoptr(SessionData) session = NULL;
	g_autoptr(GError) error = NULL;

	if (!_fprint_device_check_claimed (rdev, invocation, STATE_CLAIMED, &error)) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		return TRUE;
	}

	session = session_data_get (priv);

	delete_enrolled_fingers (rdev, session->username);
	fprint_dbus_device_complete_delete_enrolled_fingers2 (dbus_dev,
							      invocation);
	return TRUE;
}

static gboolean
handle_unauthorized_access (FprintDevice *rdev,
			    GDBusMethodInvocation *invocation,
			    GError *error)
{
	FprintDevicePrivate *priv = fprint_device_get_instance_private (rdev);

	g_assert (error);

	g_warning ("Client %s not authorized for device %s: %s",
		   g_dbus_method_invocation_get_sender (invocation),
		   fp_device_get_name (priv->dev),
		   error->message);
	g_dbus_method_invocation_return_gerror (invocation, error);

	return FALSE;
}

static gboolean
action_authorization_handler (GDBusInterfaceSkeleton *interface,
			      GDBusMethodInvocation *invocation,
			      gpointer user_data)
{
	FprintDBusDevice *dbus_dev = FPRINT_DBUS_DEVICE (interface);
	FprintDevice *rdev = FPRINT_DEVICE (dbus_dev);
	FprintDevicePrivate *priv = fprint_device_get_instance_private (rdev);
	FprintDeviceClaimState required_state = STATE_IGNORED;
	FprintDevicePermission required_perms = FPRINT_DEVICE_PERMISSION_NONE;
	gboolean needs_user_auth = FALSE;
	g_autoptr(GError) error = NULL;
	const gchar *method_name;

	method_name = g_dbus_method_invocation_get_method_name (invocation);

	g_debug ("Requesting device '%s' authorization for method %s from %s",
		 fp_device_get_name (priv->dev), method_name,
		 g_dbus_method_invocation_get_sender (invocation));

	if (g_str_equal (method_name, "Claim")) {
		needs_user_auth = TRUE;
		required_state = STATE_UNCLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_VERIFY;
		required_perms |= FPRINT_DEVICE_PERMISSION_ENROLL;
	} else if (g_str_equal (method_name, "DeleteEnrolledFingers")) {
		needs_user_auth = TRUE;
		required_perms |= FPRINT_DEVICE_PERMISSION_ENROLL;
	} else if (g_str_equal (method_name, "DeleteEnrolledFingers2")) {
		required_state = STATE_CLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_ENROLL;
	} else if (g_str_equal (method_name, "EnrollStart")) {
		required_state = STATE_CLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_ENROLL;
	} else if (g_str_equal (method_name, "EnrollStop")) {
		required_state = STATE_CLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_ENROLL;
	} else if (g_str_equal (method_name, "ListEnrolledFingers")) {
		needs_user_auth = TRUE;
		required_perms |= FPRINT_DEVICE_PERMISSION_VERIFY;
	} else if (g_str_equal (method_name, "Release")) {
		required_state = STATE_CLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_VERIFY;
		required_perms |= FPRINT_DEVICE_PERMISSION_ENROLL;
	} else if (g_str_equal (method_name, "VerifyStart")) {
		required_state = STATE_CLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_VERIFY;
	} else if (g_str_equal (method_name, "VerifyStop")) {
		required_state = STATE_CLAIMED;
		required_perms |= FPRINT_DEVICE_PERMISSION_VERIFY;
	} else {
		g_assert_not_reached ();
	}

	/* This is just a quick check in order to avoid authentication if
	 * the user cannot make the call at this time anyway.
	 * The method handler itself is required to check again! */
	if (!_fprint_device_check_claimed (rdev, invocation, required_state,
					   &error)) {
		return handle_unauthorized_access (rdev, invocation, error);
	}

	if (needs_user_auth &&
	    !fprintd_device_authorize_user (rdev, invocation, &error)) {
		return handle_unauthorized_access (rdev, invocation, error);
	}

	/* This may possibly block the invocation till the user has not
	 * provided an authentication method, so other calls could arrive */
	if (!fprint_device_check_polkit_for_permissions (rdev, invocation,
							 required_perms,
							 &error)) {
		return handle_unauthorized_access (rdev, invocation, error);
	}

	g_debug ("Authorization granted to %s for device %s!",
		 fp_device_get_name (priv->dev),
		 g_dbus_method_invocation_get_sender (invocation));

	return TRUE;
}

static void fprint_device_dbus_skeleton_iface_init (FprintDBusDeviceIface *iface)
{
	iface->handle_claim = fprint_device_claim;
	iface->handle_delete_enrolled_fingers = fprint_device_delete_enrolled_fingers;
	iface->handle_delete_enrolled_fingers2 = fprint_device_delete_enrolled_fingers2;
	iface->handle_enroll_start = fprint_device_enroll_start;
	iface->handle_enroll_stop = fprint_device_enroll_stop;
	iface->handle_list_enrolled_fingers = fprint_device_list_enrolled_fingers;
	iface->handle_release = fprint_device_release;
	iface->handle_verify_start = fprint_device_verify_start;
	iface->handle_verify_stop = fprint_device_verify_stop;
}

